![Comando tree](logo.png "Comando tree")

# Manual de Usuario 

## Aplicacion a usar Squirrelmail - uso Webmail

- Paso 1 : El dueño del server debe registrarlo por medio de consola y activarlo enviando un correo

- Paso 2 : Ingrese al siguiente sitio :

        ceos.isw612.xyz/mail

- Paso 3 : Digite sus credenciales y presione `Login`

![Comando tree](img27.png "Comando tree")

- Paso 3 : Puede enviar correos desde el siguiente boton `Compose`

![Comando tree](img28.png "Comando tree")

- Paso 4 : Puede reenviar correos desde el siguiente boton `Forward`


![Comando tree](img29.png "Comando tree")

- Paso 5 : Puede ver la bandeja de entrada desde el siguiente boton `INBOX`

![Comando tree](img30.png "Comando tree")

- Paso 6 : Puede ver la bandeja de salida desde el siguiete boton `sent`

![Comando tree](img31.png "Comando tree")

- Paso 7 : En el siguiente boton `delete` puede eliminar un correo:


![Comando tree](img32.png "Comando tree")


- Paso 8 : Se puede volver a loguear del siguiente boton:


![Comando tree](img34.png "Comando tree")

- Paso 9 : Se puede desloguear del siguiente boton `Sign Out`


![Comando tree](img33.png "Comando tree")
 
Paso 10 : En la imagen adjunta se especifica como debe rellenar los espacios para enviar un correo

![Comando tree](img35.png "Comando tree")

Paso 11 : En el siguiente boton puede enviar el correo:

![Comando tree](img36.png "Comando tree")