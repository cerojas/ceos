
![Comando tree](logo.png "Comando tree")


# INSTALACION DE SERVIDOR DE COORREOS

## DOCUMENTACION TECNICA 

### INSTALACION DE LA MAQUINA VIRTUAL EN AZURE

- Paso 1 :  Creación de un grupo de recursos en Azure

![Comando tree](img01.png "Comando tree")

- Paso 2 : Crear máquina Virtual en Azure

![Comando tree](img02.png "Comando tree")

- Paso 3 :	Configuracion de la Maquina Virtual

![Comando tree](img03.png "Comando tree")

![Comando tree](img04.png "Comando tree")

- Paso 4 : Seleccionar el disco que tendra la maquina virtual

![Comando tree](img05.png "Comando tree")

- Paso 5 : Selección de Redes , todo previamente configurado

![Comando tree](img06.png "Comando tree")

- Paso 6 : Selección de Administracion, en esta pestaña desactiva el `Diagnostico de arranque`

![Comando tree](img07.png "Comando tree")

- Paso 7 : Selección de etiquetas tales como :

        Centro_de_Costos
        Tipo

![Comando tree](img08.png "Comando tree")

- Paso 8 : Creacion de la maquina virtual

![Comando tree](img09.png "Comando tree")

        Ejemplo de maquina creada :
   
![Comando tree](img10.png "Comando tree")   

- Paso 9 : Configuracion de la ip estatica

![Comando tree](img11.png "Comando tree")

![Comando tree](img12.png "Comando tree")

- Paso 10 : Descargar e iniciar la maquina virtual por medio de `PUTTY`

![Comando tree](img13.png "Comando tree")

### CONFIGURACION DE LA MAQUINA VIRTUAL EN SISTEMA OPERATIVO UBUNTO

- Paso 1 : Iniciar con su usuario y contraseña

![Comando tree](img14.png "Comando tree")

- Paso 2 : Actuaizar la maquina con el comando `apt update`

![Comando tree](img15.png "Comando tree")

- Paso 3 : Iniciar en modo de administrador utilizando el siguiente comando `sudo su`


![Comando tree](img16.png "Comando tree")

- Paso 4 : Instalar apache con el siguiente comando `apt-get install apache2`

![Comando tree](img17.png "Comando tree")

- Para reiniciar apache2 utilizar el siguiente comando:

        /etc/init.d/apache2 restart

- Paso 5 : Instalar php con el siguiente comando `apt-get install php`

- Paso 6 : Instalar el servidor courier pop con el siguiente comando:

        apt-get install courier-pop

- Paso 7 : Instalar el servidor courier imap con el siguiente comando:

        apt-get install courier-imap

- Paso 8 : Crear usuarios con el siguiente comando :

        adduser nombredeusuario

- Paso 9 : Activar el usuario enviando un correo con los comandos :

         mail correo@gmail.com
         rellenar lo solicitado
         Ctrl + D para enviar
  

## Intalar postfix Ubuntu

- Paso 1 : Instalar postfix ejecutando el siguiente comando :

        apt-get install postfix

- Paso 2 : Seleccionar la opcion `Sitio de Internet` como se muestra en la imagen        

![Comando tree](img24.png "Comando tree")

- Paso 3 :En la siguiente pantalla pedirá que se escriba el dominio del correo de electrónico en este caso sera ceos.isw612.xyz


![Comando tree](img25.png "Comando tree")

- Paso 4 : Al aceptar el nombre del sistema de correo iniciará la carga de las configuraciones básicas, terminado se debe ejecutar el comando  nano /etc/postfix/main.cf.

![Comando tree](img26.png "Comando tree")

Verficar y configurar lo que se recuadra en la imagen



- Paso 5 : Reiniciar postfix cada vez que se realiza un cambio con el siguiente comando:

        /etc/init.d/postfix/ restart


## Instalar SquirrelMail Ubuntu

- Paso 1 : Primero, cd a la carpeta `/ tmp` escribiendo el siguiente comando:

        cd / tmp

- Paso 2 : Luego, ingrese el siguiente comando para descargar SquirrelMail

        wget http : //downloads.sourceforge.net/project/squirrelmail/stable/1.4.22/squirrelmail-webmail-1.4.22.zip

- Paso 3 :instalemos la herramienta de descompresión en nuestro servidor Ubuntu 18.04:

        apt-get install descomprimir

- Paso 4 :  necesitamos descomprimir el archivo SquirrelMail usando el siguiente comando:

        descomprimir squirrelmail-webmail- 1.4 .22 .zip

- Paso 5 :      trasladamos el contenido del directorio `squirrelmail-webmail-1.4.22` a la raíz de nuestro sitio web:

        mv squirrelmail-webmail- 1.4 .22 / / var / www / html / mail

- Paso 6 :    Para que Apache pueda interactuar con SquirrelMail sin problemas de lectura / escritura, debemos establecer el directorio correcto y los permisos de propiedad del archivo usando el siguiente comando:

        chown -R www-data: www-data / var / www / html / mail

 - Paso 7 : SquirrelMail tiene un archivo de configuración predeterminado ( `config_default.php` ). Necesitamos copiar este archivo a config.php usando el siguiente comando:

               cp /var/www/html/mail/config/config_default.php /var/www/html/mail/config/config.php

- Paso 8 :Luego necesitamos editar el archivo usando nano editor para hacer algunos cambios:

        nano /var/www/html/mail/config/config.php


 - Paso 9 : Necesitamos establecer los valores a continuación:

        $domain = 'yourwebsite.com';
        $data_dir = '/var/www/html/mail/data/';
        $attachment_dir = '/var/www/html/mail/attach/';

 - Paso 10 : El directorio de datos se crea de manera predeterminada una vez que instala SquirrelMail, pero necesitamos crear el directorio de archivos adjuntos utilizando el siguiente comando:

        /var/www/html/mail/attach/


 - Paso 11 : Podemos ejecutar el siguiente comando una vez más para garantizar que los permisos se actualicen en el directorio:

        chown -R www-data:www-data /var/www/html/mail       

 Paso 12 : Puede visitar su aplicación SquirrelMail utilizando la dirección 'www.example.com/mail' . Si la configuración se completó sin problemas, debería ver esta página de inicio de sesión:   


 ![Comando tree](img23.png "Comando tree")



## Crear Cuenta en SendGrid

- Primero, regístrese para recibir su prueba gratuita en SendGrid.com. Luego verifique su cuenta al haga clic en el email de confirmación que recibió.

        https://sendgrid.com/


![Comando tree](img19.png "Comando tree")


- Luego, usted necesitará generar su SendGrid API key. De clic en “Opciones” -> “API Keys.” Luego de clic en `Crear API Key.`

![Comando tree](img20.png "Comando tree")

- De a su API un nombre que pueda reconocer. Esto puede ser cualquier cosa. Seleccione “Acceso Total” y haga clic en `Crear & Ver.`


![Comando tree](img21.png "Comando tree")

- Usted querrá copiar su API key y guardarlo en un lugar seguro ya que no se lo volverán a mostrar. Usted necesitará esto para configurar los plugins de abajo


![Comando tree](img22.png "Comando tree")

- Para entrar con el dominio ceos.isw612.xyz ingresar los siguientes comandos:

                /etc/apache2/sites-available
                nano 000-default.conf 


## Configurar SendGrid en Ubuntu

- Instala el agente de transporte de correos Postfix:

        apt-get update && apt-get install postfix libsasl2-modules 

- Modifica las opciones de configuración de Postfix. Abre /etc/postfix/main.cf para edición. Por ejemplo, para usar el editor de texto nano, ingresa el comando siguiente:

        nano /etc/postfix/main.cf

- Agrega las siguientes líneas al final del archivo:

        mtpd_sasl_local_domain = $myhostname
        smtpd_recipient_restrictions = permit_sasl_authenticated, permit_mynetworks, reject_unauth_destination
        mailbox_size_limit = 256000000

        # Sendgrid Settings
        smtp_sasl_auth_enable = yes
        smtp_sasl_password_maps = static:apikey:SG.YOUR_SENDGRID_KEY
        smtp_sasl_security_options = noanonymous
        smtp_tls_security_level = may
        header_size_limit = 4096000
        relayhost = [smtp.sendgrid.net]:587

![Comando tree](img18.png "Comando tree")

- Genera el mapa de contraseña SASL con la clave de API generada en la sección Antes de comenzar con el comando siguiente:

        echo [smtp.sendgrid.net]:587 apikey:[YOUR_API_KEY] >> /etc/postfix/sasl_passwd

- Usa la utilidad postmap para generar un archivo .db con el comando siguiente:

        postmap /etc/postfix/sasl_passwd

- Verifica que tengas un archivo .db de la manera siguiente:

        ls -l /etc/postfix/sasl_passwd*

- Quita el archivo que contiene tus credenciales, ya que no lo necesitarás con el comando siguiente:

        rm /etc/postfix/sasl_passwd

- Configura los permisos en tu archivo .db y verifica que el otro archivo se haya quitado de la manera siguiente:

        chmod 600 /etc/postfix/sasl_passwd.db
        ls -la /etc/postfix/sasl_passwd.db

- Vuelve a cargar la configuración para cargar los parámetros modificados con los siguientes comandos

        /etc/init.d/postfix restart

- Instala el paquete mailutils o mailx con los comandos siguientes:

         apt-get install mailutils

- Enviar correos de prueba con los siguientes pasos desde la linea de comandos: :

        mail correo@gmail.com
        rellenar lo solicitado
        Ctrl + D para enviar

- Revisar los log con el siguiente comando :

        tail -n 5 /var/log/syslog



